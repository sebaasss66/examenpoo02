/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pagoEmpleados;

/**
 *
 * @author sebas
 */
public class pagoEmpleados {
    
    private int numDosentes;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBase;
    private int horasImpartidas;
    
    
    public pagoEmpleados(){
        this.numDosentes=0;
        this.nombre="";
        this.domicilio="";
        this.nivel=0;
        this.pagoBase=0.0f;
        this.horasImpartidas=0;
    }

    public pagoEmpleados(int numDosentes, String nombre, String domicilio, int nivel, float pagoBase, int horasImpartidas) {
        this.numDosentes = numDosentes;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBase = pagoBase;
        this.horasImpartidas = horasImpartidas;
    }
    
    public pagoEmpleados(pagoEmpleados otro){
        this.numDosentes = otro.numDosentes;
        this.nombre = otro.nombre;
        this.domicilio = otro.domicilio;
        this.nivel = otro.nivel;
        this.pagoBase = otro.pagoBase;
        this.horasImpartidas = otro.horasImpartidas;
    }

    public int getNumDosentes() {
        return numDosentes;
    }

    public void setNumDosentes(int numDosentes) {
        this.numDosentes = numDosentes;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
    }
    
    public float calcularPago(){
       float pagoBasePorHora = this.pagoBase;

    switch (this.nivel) {
        case 1:
            pagoBasePorHora *= 1.3f; 
            break;
        case 2:
            pagoBasePorHora *= 1.5f; 
            break;
        case 3:
            pagoBasePorHora *= 2.0f; 
            break;
        default:
            break;
    }

    return pagoBasePorHora * this.horasImpartidas; 
    }
    
    public float calcularImpuesto(){
        float pagoTotal = this.calcularPago();
        
        return pagoTotal * 0.16f; 
    }
    
    public float calcularBono(int numHijos) {
    float bono = 0;

    if (numHijos >= 1 && numHijos <= 2) {
        bono = this.calcularPago() * 0.05f; 
    } else if (numHijos >= 3 && numHijos <= 5) {
        bono = this.calcularPago() * 0.10f; 
    } else if (numHijos > 5) {
        bono = this.calcularPago() * 0.20f; 
    }

    return bono;
}

    
    
}
